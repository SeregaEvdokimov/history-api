/**
 * Created by G551 on 01.12.2016.
 */

var links,
    contentEl,
    navEl;

contentEl = document.querySelector('.content');
navEl = document.querySelector('.nav');

links = {
    main: 'This is the <strong>MAIN</strong> page',
    about: 'This is the <strong>ABOUT</strong> page',
    downloads: 'This is the <strong>DOWNLOADS</strong> page'
};

function updateState(state) {
    if(!state) return false;
    updateButton(state);
    contentEl.innerHTML = links[state.page];
}

function updateButton(state) {
    [].slice.apply(navEl.querySelectorAll('a')).forEach(function(item) {
        var classList = item.parentNode.classList;
       state.page === item.getAttribute('href')
           ? classList.add('active')
           : classList.remove('active');
    });
}

window.addEventListener('popstate', function(e) {
    updateState(e.state);
});

navEl.addEventListener('click', function(e) {
    e.preventDefault();
    if(e.target.tagName !== 'A') return false;


    var state;
    var href = e.target.getAttribute('href');

    state = {
        page: href
    };

    history.pushState(state, '', state.page);
    updateState(state);
});


// window.addEventListener('hashchange', updateState);
// window.addEventListener('load', updateState);